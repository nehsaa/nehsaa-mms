crumb :root do
  link I18n.t("ui.home"), root_path
end

crumb :members do
  link Member.model_name.human, members_path
end

crumb :member do |member|
  link member.name, member_path(member)
  parent :members
end

crumb :edit_member do |member|
  link I18n.t("ui.edit_member"), edit_member_path(member)
  parent :member, member
end

crumb :new_member do
  link I18n.t("ui.new_member"), new_member_path
  parent :members
end

crumb :memberships do |member|
  link Membership.model_name.human, member_memberships_path
  parent :member, member
end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).