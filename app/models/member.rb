class Member < ActiveRecord::Base
  extend TranslatedEnums

  enum gender: { male: 1, female: 2, other: 3 }

  enum member_type: {
    normal_member: 1,
    staff_member: 2,
    honor_member: 3
  }

  enum grad_department: {
    kindergarten: 1,
    elementary: 2,
    junior_high: 3,
    senior_high: 4,
    bilingual: 5
  }

  validates :name, :security_id,
            :birthdate, :gender, :member_type,
            :email, :phone, :address,
            :occupation, :company, :education,
            { presence: true }
  validates :security_id,     security_id: true, uniqueness: true
  validates :permanent,       inclusion: { in: [true, false] }
  validates :email,           email: true

  has_many :memberships, dependent: :delete_all

  def age(date = Date.today)
    date.year - birthdate.year
  end

  def membership?(date = Date.today)
    flag = memberships.count > 0
    if flag
      flag = memberships.order(year: :asc).last.year >= (date.year - 2)
    end
    valid? && (age(date) < 20 || flag || permanent)
  end

end
