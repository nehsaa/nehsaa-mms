module OldWorld
  
  def self.table_name_prefix
    'member_profile_'
  end
  
  class Base < ActiveRecord::Base
    self.abstract_class = true
    #def readonly?; true; end
    
    establish_connection Rails.configuration.database_configuration['old_world']
  end
  
  class Graduation < Base
  end
  
  class Kind < Base
  end
  
  class Department < Base
  end
  
  class Gender < Base
  end
  
  class Payment < Base
  end
  
  class Member < Base
    
    belongs_to :gender
    belongs_to :kind
    
    %w(emails phones addresses facebook_accounts
       educations organizations occupations
    ).each {|name|
      class_eval <<-RUBY
        class #{name.classify} < Base
          belongs_to :member
        end
        has_many :#{name}, dependent: :destroy
      RUBY
    }
    %w(graduations payments).each {|name|
      has_many name.to_sym, dependent: :destroy
    }
  end
  
  class SchemaMigration < Base
    self.table_name = 'schema_migrations'
    self.primary_key = 'version'
  end
  
end

OldWorld::Member.class_eval do
  def to_new_world
    new_world_member = ::Member.new({
      name: self.name,
      birthdate: self.birthday,
      security_id: self.national_identification_number,
      gender: self.gender_id,
      member_type: self.kind_id,
      permanent: !!self.is_lifelong
    })
    
    self.graduations.each do |g|
      new_world_member.grad_class = g.class_number
      new_world_member.grad_year = g.year
      new_world_member.grad_id = g.school_number
      new_world_member.grad_department = g.department_id
    end
    
    self.payments.each do |p|
      new_world_member.memberships.build({
        year: p.membership_year
      })
    end
    
    new_world_member.email = self.emails.each.map{|o| o.value }.join(', ')
    new_world_member.phone = self.phones.each.map{|o| o.value }.join(', ')
    new_world_member.address = self.addresses.each.map{|o| o.value }.join(', ')
    new_world_member.facebook = self.facebook_accounts.each.map{|o| o.value }.join(', ')
    new_world_member.occupation = self.occupations.each.map{|o| o.value }.join(', ')
    new_world_member.company = self.organizations.each.map{|o| o.value }.join(', ')
    new_world_member.education = self.educations.each.map{|o| o.value }.join(', ')
    
    new_world_member
  end
end

module OldWorld
  
  def self.detect_duplicates
    old_logger = Base.logger
    Base.logger = nil
    
    ::OldWorld::Member.find_each(batch_size: 50) {|m|
      x = m.national_identification_number
      unless x.blank?
        n = ::OldWorld::Member.where(national_identification_number: x)
        if (n.count != 1)
          puts "== Duplicate detected =="
          puts "SID = #{ x }"
          n.each do |mi|
            puts "NAME: #{ mi.name }"
          end
          puts "------------------------"
        end
      else
        puts "ID= #{m.id}, NAME= #{m.name} has no Security ID"
      end
    }
    
    Base.logger = old_logger
    nil
  end
  
  def self.copy_to_new_world(validate = true)
    old_logger = Base.logger
    Base.logger = nil
    
    r = ::OldWorld::Member.includes(
      :graduations, :payments,
      :emails, :addresses, :phones, :facebook_accounts,
      :organizations, :educations, :occupations
    )
    invalid_ids = []
    
    r.find_each(batch_size: 50) {|old_world_member|
      new_world_member = old_world_member.to_new_world
      invalid_ids << old_world_member.id unless new_world_member.valid?
      if new_world_member.security_id.blank?
        puts "The member(id=#{ old_world_member.id }, name=#{ old_world_member.name }) has no Security ID"
      else
        unless new_world_member.save(validate: validate)
          puts "======="
          puts "ID(old_world) = #{old_world_member.id}"
          puts "errors = #{new_world_member.errors.inspect}"
          puts "-------"
        end
      end
    }
    
    Base.logger = old_logger
    return invalid_ids
  end
  
end