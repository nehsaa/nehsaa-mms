module TranslatedEnums
  extend ActiveSupport::Concern
  
  def enum(hash)
    super
    hash.keys.map(&:to_sym).each do |name|
      key = ["activerecord.enums", model_name.i18n_key, name].join(".")
      define_method(:"#{name}_translated") do
        val = send(name)
        val && I18n.t("#{ key }.#{ val }")
      end
      alias_method :"#{name}_tr", :"#{name}_translated"
    end
  end
end
