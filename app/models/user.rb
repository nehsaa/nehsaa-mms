class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :recoverable, :registerable,
  # :rememberable, :timeoutable and :omniauthable
  devise :database_authenticatable, :trackable, :validatable
end
