class MembersController < ApplicationController
  before_action :authenticate_user!

  def index
    @members = Member.all.includes(:memberships)
    @member_grid = initialize_grid(@members)
  end

  def new
    @member = Member.new
  end

  def create
    @member = Member.create(member_params)
    if @member.valid?
      redirect_to @member
    else
      render action: 'new'
    end
  end

  def show
    @member = Member.find(params[:id])
  end

  def edit
    @member = Member.find(params[:id])
  end

  def update
    @member = Member.update(params[:id], member_params)
    if @member.valid?
      redirect_to @member
    else
      render action: 'edit'
    end
  end

  def destroy
    Member.destroy(params[:id])
    redirect_to action: 'index'
  end

  def moi_sheet
    @members = Member.all
    #response.headers['Content-Type'] = 'text/html; charset=UTF-8'
    #response.headers['Content-Disposition'] = 'attachment; filename="moi-sheet.html"'
    render layout: false, stream: true
  end

  private

  def allowed_attributes
    %i(name security_id birthdate gender
       member_type permanent
       email phone address occupation company education facebook
       grad_class grad_year grad_id grad_department)
  end

  def member_params
    unless @member_params
      @member_params = params.require(:member)
      @member_params = @member_params.permit(*allowed_attributes)
    end
    @member_params
  end

end
