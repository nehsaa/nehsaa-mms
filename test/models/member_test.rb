require "test_helper"

class MemberTest < ActiveSupport::TestCase

  def member
    if not @member
      @member = Member.new
      @member.name = 'Mesaki Mei'
      @member.security_id = 'K2xxxxxxx'
      @member.birthdate = Date.today
      @member.gender = :female
      @member.member_type = :normal_member
      @member.permanent = false
      @member.email = 'mmei@momochrome.co.jp'
      @member.phone = '0325321111'
      @member.address = 'Kaminarimon 3-2-1'
      @member.occupation = 'Student'
      @member.company = 'Tokyo University'
      @member.education = 'Tokyo University'
      @member.facebook = nil
      @member.grad_class = 10
      @member.grad_year = 1999
      @member.grad_id = nil
      @member.grad_department = :senior_high
    end
    @member
  end

  def test_valid
    assert member.valid?
  end

end
